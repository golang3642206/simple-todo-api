package migrations

import (
	"fmt"
	"os"

	_ "github.com/joho/godotenv/autoload"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var (
	DB_HOST = os.Getenv("DB_HOST")
	DB_NAME = os.Getenv("DB_NAME")
	DB_PORT = os.Getenv("DB_PORT")
	DB_USER = os.Getenv("DB_USER")
	DB_PASS = os.Getenv("DB_PASS")
)

func InitDB() *gorm.DB {
	// dsn := "user:password@tcp(127.0.0.1:3306)/database_name?charset=utf8mb4&parseTime=True&loc=Local"
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("Failed to connect to the database")
	}
	return db
}

func Migrate(db *gorm.DB) {
	InitDB()
	// if db.AutoMigrate(
	// 	&Todo{},
	// ) != nil {
	// 	panic("Failed to migrate")
	// }

	fmt.Println("Checking database migrations...")
	if !db.Migrator().HasTable(&Todo{}) {
		fmt.Println("Running database migrations.")
		// if err := db.AutoMigrate(&Todo{}); err != nil {
		// 	panic("Failed to migrate")
		// }
		err := db.AutoMigrate(&Todo{})
		if err != nil {
			panic("Failed to migrate: " + err.Error())
		}
		
		fmt.Println("Database migrations completed.")
	} else {
		fmt.Println("Database is already migrated.")
	}
}
