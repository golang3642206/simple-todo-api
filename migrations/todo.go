package migrations

import (
	"gorm.io/gorm"
)

type Todo struct {
	gorm.Model
	TaskName         string `gorm:"not null"`
	IsComplete       bool   `gorm:"default:false"`
}

