package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	"simple-todo-api/models"
)

type CreateTodoRequest struct {
	TaskName string `json:"taskName" binding:"required"`
}

// @Summary Get all todos
// @Description Get all todos
// @Tags Todos
// @Produce json
// @Success 200 {array} models.Todo
// @Router /api/todos [get]
func GetAllTodos(c *gin.Context) {
	todos, err := models.GetAllTodos()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to retrieve todos"})
		return
	}
	fmt.Printf("todos: %v\n", todos)
	c.JSON(http.StatusOK, todos)
}

// @Summary Get a todo by ID
// @Description Get a todo item by its ID
// @ID get-todo-by-id
// @Tags Todos
// @Produce json
// @Param id path integer true "Todo ID"
// @Success 200 {object} models.Todo
// @Failure 400 "Invalid ID format"
// @Failure 404 "Not found"
// @Router /api/todo/{id} [get]
func GetTodoById(c *gin.Context) {
	idString := c.Param("id")

	id, err := strconv.ParseUint(idString, 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid ID format"})
		return
	}

	todo, err := models.GetTodoById(uint(id))
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Not found"})
		return
	}

	c.JSON(http.StatusOK, todo)
}

// @Summary Create a new todo
// @Description Create a new todo with the provided task name
// @ID create-todo
// @Tags Todos
// @Accept json
// @Produce json
// @Param request body CreateTodoRequest true "Request body for creating a new todo"
// @Success 201 "OK"
// @Failure 500 "Internal Server Error"
// @Router /api/todo [post]
func CreateTodo(c *gin.Context) {
	var requestBody CreateTodoRequest

	if err := c.ShouldBindJSON(&requestBody); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	_, err := models.CreateTodo(requestBody.TaskName)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create todo: " + err.Error()})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"message": "OK"})
}

// @Summary Update the status of a todo by ID
// @Description Update the status of a todo (complete or incomplete) by providing its ID
// @ID update-todo-status
// @Tags Todos
// @Accept json
// @Produce json
// @Param id path uint true "Todo ID" Format(uint64)
// @Success 200 "OK"
// @Failure 400 "Invalid ID format"
// @Failure 500 "Internal Server Error"
// @Router /api/todo/{id} [patch]
func UpdateTodoStatusById(c *gin.Context) {
	idString := c.Param("id")
	id, err := strconv.ParseUint(idString, 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid ID format"})
		return
	}
	if err := models.UpdateTodoStatusById(uint(id)); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update todo status: " + err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "OK"})
} 

// @Summary Delete a todo by ID
// @Description Delete a todo by providing its ID
// @ID delete-todo
// @Tags Todos
// @Accept json
// @Produce json
// @Param id path uint true "Todo ID" Format(uint64)
// @Success 200 "OK"
// @Failure 400 "Invalid ID format"
// @Failure 500 "Internal Server Error"
// @Router /api/todo/{id} [delete]
func DeleteTodoById(c *gin.Context) {
	idString := c.Param("id")
	id, err := strconv.ParseUint(idString, 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid ID format"})
		return
	}
	if err := models.DeleteTodoById((uint(id))); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete todo: " + err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "OK"})
}