package routes

import (
	"net/http"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/gin-gonic/gin"

	"simple-todo-api/controllers"
)

type Api struct{}
// LoadRoute is a method of Api struct
func (a *Api) LoadRoute(e *gin.Engine) {
	swagger(e)
	api(e)
}

func swagger(e *gin.Engine) {
	e.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
  e.GET("/swagger", func(ctx *gin.Context) {
			ctx.Redirect(http.StatusTemporaryRedirect, "/swagger/index.html")
	})
}

func api(e *gin.Engine){
	api := e.Group("/api")
	{
		api.GET("/todos", controllers.GetAllTodos)
		api.GET("/todo/:id", controllers.GetTodoById)
		api.POST("/todo", controllers.CreateTodo)
		api.PATCH("/todo/:id", controllers.UpdateTodoStatusById)
		api.DELETE("/todo/:id", controllers.DeleteTodoById)
	}
}