package models

import (
	"fmt"
	"gorm.io/gorm"
	"simple-todo-api/migrations"
)

var db *gorm.DB

func SetDB(database *gorm.DB) {
	db = database
}

type Todo migrations.Todo

func GetAllTodos() ([]Todo, error) {
	var todos []Todo
	if err := db.Find(&todos).Error; err != nil {
		return nil, err
	}
	return todos, nil
}

func GetTodoById(id uint) (*Todo, error) {
	var todo Todo

	if err := db.First(&todo, id).Error; err != nil {
		return nil, err
	}
	fmt.Printf("todo: %v\n", todo)
	return &todo, nil
}

func CreateTodo(taskName string) (*Todo, error) {
	todo := &Todo {
		TaskName: taskName,
	}

	if err := db.Create(todo).Error; err != nil {
		return nil, err
	}

	return todo, nil
}

func UpdateTodoStatusById(id uint) (error) {
	var todo Todo
	getResult := db.First(&todo, id)
	if getResult.Error != nil {
		return getResult.Error
	}

	fmt.Printf("getResult: %v\n", getResult)
	fmt.Printf("todo: %v\n", todo)

	todo.IsComplete = !todo.IsComplete

	saveResult := db.Save(&todo)
	return saveResult.Error
}

func DeleteTodoById(id uint) (error) {
	return db.Delete(&Todo{}, id).Error
}