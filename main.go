package main

import (
	// "fmt"
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/joho/godotenv"

	// swagger docs
	_ "simple-todo-api/docs"

	"simple-todo-api/migrations"
	"simple-todo-api/models"
	"simple-todo-api/routes"
)

// @title						SIMPLE TODO API
// @version				  v0.1.0
// @description     A simple api for todo list management
// @BasePath			  /api
func main() {
	// Connect to DB
	db := migrations.InitDB()
	migrations.Migrate(db)

	models.SetDB(db)
	
	// Create a default gin.Engine
	e := gin.Default()
	
	routes := routes.Api{}
	routes.LoadRoute(e)

	port := os.Getenv("WEB_PORT")
	e.Run(":" + port)
}