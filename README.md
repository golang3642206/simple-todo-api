# simple-todo-api

### Dev

start the go project
```
go mod init simple-todo-api
```

package for gin (web server)
```
go get -u github.com/gin-gonic/gin
```

package for SQL & gorm
```
go get -u gorm.io/gorm
go get -u gorm.io/driver/mysql
```

package for .env
```
go get -u github.com/joho/godotenv
```

Swagger
```
swag init --parseDependency
```
`docs` folder will be created after `swag init`

Run web server
```
# go run main.go
go run .
```

### Run by yourself
```
# git clone & cd the project folder
go mod tidy
go build
./simple-todo-api
```